package com.example.android.quakereport;

/**
 * Created by andrewijaya on 6/26/17.
 */

public class Earthquake {
    private double mMagnitude;
    private String mLocation;
    private long mDate;

    public String getmURL() {
        return mURL;
    }

    public Earthquake(double mMagnitude, String mLocation, long mDate, String mURL) {
        this.mMagnitude = mMagnitude;
        this.mLocation = mLocation;
        this.mDate = mDate;
        this.mURL = mURL;

    }

    private String mURL;


    public double getmMagnitude() {
        return mMagnitude;
    }

    public String getmLocation() {
        return mLocation;
    }

    public long getmDate() {
        return mDate;
    }
}
